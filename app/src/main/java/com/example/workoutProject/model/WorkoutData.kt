package com.example.workoutProject.model


class WorkoutData (
    private var title: String? = null,
    private var set: Int? = null,
    private var prep: Int? = null,
    private var workout: Int? = null,
    private var rest: Int? = null
) {

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getSet(): Int? {
        return set
    }

    fun setSet(set: Int?) {
        this.set = set
    }

    fun getPrep(): Int? {
        return prep
    }

    fun setPrep(prep: Int?) {
        this.prep = prep
    }

    fun getWorkout(): Int? {
        return workout
    }

    fun setWorkout(workout: Int?) {
        this.workout = workout
    }

    fun getRest(): Int? {
        return rest
    }

    fun setRest(rest: Int?) {
        this.rest = rest
    }

}