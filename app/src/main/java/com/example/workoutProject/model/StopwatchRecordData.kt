package com.example.workoutproject.data

class StopwatchRecordData (
        private var title: String? = null,
        private var record: String? = null
) {

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getRecord(): String? {
        return record
    }

    fun setRecord(record: String?) {
        this.record = record
    }

}