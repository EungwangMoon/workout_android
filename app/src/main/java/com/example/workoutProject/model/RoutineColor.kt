package com.example.workoutproject.data

enum class RoutineColor {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    NAVY,
    PURPLE,
    DARK_GRAY,
    LIGHT_GRAY,
    WHITE
}