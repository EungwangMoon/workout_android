package com.example.workoutProject.fragment.fourth

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.main.MainActivity
import com.example.workoutproject.R
import kotlinx.android.synthetic.main.fragment_fourth.*

class FourthFragment : BaseFragment() {

    private val TAG = javaClass.simpleName
    lateinit var mContext: Context

    val stateKey = "IS_STOPWATCH"
    var isStopwatch: Boolean = false

    companion object {
        var instance: FourthFragment? = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.e(TAG, "onCreateView")
        return inflater.inflate(R.layout.fragment_fourth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e(TAG, "onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.e(TAG, "onActivityCreated")
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            replaceFragment(TimerFragment(), R.id.fourth_content_frame, "timer")
        } else {
            isStopwatch = savedInstanceState.getBoolean(stateKey)
            if (!isStopwatch)
                replaceFragment(TimerFragment(), R.id.fourth_content_frame, "timer")
            else
                replaceFragment(StopwatchFragment(), R.id.fourth_content_frame, "stopwatch")
        }
        // top right button setOnClickListener
        fourth_top_right_button.setOnClickListener {
            if (fourth_top_right_button.text == "Stop Watch") {
                fourth_top_title_text.text = "Stop Watch"
                fourth_top_right_button.text = "Timer"
                replaceFragment(StopwatchFragment(), R.id.fourth_content_frame, "stopwatch")
            } else {
                fourth_top_title_text.text = "Timer"
                fourth_top_right_button.text = "Stop Watch"
                replaceFragment(TimerFragment(), R.id.fourth_content_frame, "timer")
            }
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(stateKey, isStopwatch)
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
}