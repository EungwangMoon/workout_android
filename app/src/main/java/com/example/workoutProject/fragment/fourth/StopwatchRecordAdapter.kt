package com.example.workoutProject.fragment.fourth

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutproject.R
import com.example.workoutproject.data.StopwatchRecordData

class StopwatchRecordAdapter  (val context: Context) :
        RecyclerView.Adapter<StopwatchRecordAdapter.StopwatchItemHolder>() {

    private val TAG = javaClass.simpleName
    var infoList: ArrayList<StopwatchRecordData> = arrayListOf()

//    interface ItemClick {
//        fun onClick(view: View, position: Int)
//    }
//
//    var itemClick: ItemClick? = null

    override fun onBindViewHolder(holder: StopwatchItemHolder, position: Int) {
        holder?.bind(infoList[position], context)
//        if (itemClick != null) {
//            holder?.itemView?.setOnClickListener { v ->
//                itemClick?.onClick(v, position)
//
//            }
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopwatchItemHolder {
        val view =
                LayoutInflater.from(context).inflate(R.layout.list_item_stopwatch_record, parent, false)
        return StopwatchItemHolder(view)
    }

    override fun getItemCount(): Int {
        return infoList.size
    }

    fun addLap(data: StopwatchRecordData) {
        Log.d(TAG, "infoList size : " + infoList.size)
        Log.d(TAG, "title: ${data.getTitle()}, record: ${data.getRecord()}")
        infoList.add(infoList.size, data)
        notifyItemInserted(infoList.size)
    }

    inner class StopwatchItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        var stopwatch_item_title_text: TextView = itemView!!.findViewById(R.id.stopwatch_item_title_text)
        var stopwatch_item_record_text: TextView = itemView!!.findViewById(R.id.stopwatch_item_record_text)

        fun bind(data: StopwatchRecordData, context: Context) {
            stopwatch_item_title_text.text = data.getTitle()
            stopwatch_item_record_text.text = data.getRecord()
        }
    }
}