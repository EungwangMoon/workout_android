package com.example.workoutProject.fragment.second

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutproject.R
import com.example.workoutproject.data.RoutineData
import com.example.workoutproject.data.RoutineColor

class RoutineAdapter (val context: Context) :
    RecyclerView.Adapter<RoutineAdapter.RoutineItemHolder>() {

    private val TAG = javaClass.simpleName
    var infoList: List<RoutineData> = listOf()

    interface ItemClick {
        fun onClick(view: View, position: Int)
    }

    var itemClick: ItemClick? = null

    override fun onBindViewHolder(holder: RoutineItemHolder, position: Int) {
        holder?.bind(infoList[position], context)
        if (itemClick != null) {
            holder?.itemView?.setOnClickListener { v ->
                itemClick?.onClick(v, position)

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutineItemHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.list_item_routine, parent, false)
        return RoutineItemHolder(view)
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "infoList size : " + infoList.size)
        return infoList.size
    }

    fun setCurrentWork(holdListData: List<RoutineData>) {
        infoList = holdListData
        notifyDataSetChanged()
    }

    inner class RoutineItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        var routine_item_color_view: View = itemView!!.findViewById(R.id.routine_item_color_view)
        var routine_item_title_text: TextView = itemView!!.findViewById(R.id.routine_item_title_text)
        var routine_item_round_text: TextView = itemView!!.findViewById(R.id.routine_item_round_text)
        var routine_item_second_text: TextView = itemView!!.findViewById(R.id.routine_item_second_text)

        fun bind(data: RoutineData, context: Context) {
            when (data.getTitleColor()) {
                RoutineColor.RED -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_red))
                RoutineColor.ORANGE -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_orange))
                RoutineColor.YELLOW -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_yellow))
                RoutineColor.GREEN -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_green))
                RoutineColor.BLUE -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_blue))
                RoutineColor.NAVY -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_navy))
                RoutineColor.PURPLE -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_purple))
                RoutineColor.DARK_GRAY -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_dark_gray))
                RoutineColor.LIGHT_GRAY -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_light_gray))
                RoutineColor.WHITE -> routine_item_color_view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.routine_white))
            }
            routine_item_title_text.text = data.getTitle()
            routine_item_round_text.text = data.getRound().toString()+"R"
            val totalSeconds: Int? = data.getSecond()
            if (totalSeconds != null) {
                val sec: Int = totalSeconds % 60
                val min: Int = (totalSeconds / 60) % 60
                val hour: Int = (totalSeconds / 60) / 60
                var fullTime = ""

                // h, m = 0 -> 00s
                // h = 0 -> 00:00
                if (hour == 0 && min == 0) {
                    fullTime = "${sec}s"
                } else if (hour == 0) {
                    fullTime = "${min}m ${sec}s"
                } else {
                    fullTime = "${hour}h ${min}m ${sec}s"
                }


                fullTime = ""
                if (hour > 0)
                    fullTime = fullTime+hour+"h"
                if (min > 0)
                    fullTime = fullTime+min+"m"
                if (sec > 0)
                    fullTime = fullTime+sec+"s"

                routine_item_second_text.text = fullTime
            }
        }

    }

}