package com.example.workoutProject.fragment.third

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.main.MainActivity
import com.example.workoutproject.R

class ThirdFragment : BaseFragment() {

    private val TAG = javaClass.simpleName
    lateinit var mContext: Context

    companion object {
        var instance: ThirdFragment? = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
}