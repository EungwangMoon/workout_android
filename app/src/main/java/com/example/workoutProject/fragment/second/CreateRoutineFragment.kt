package com.example.workoutProject.fragment.second

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutProject.model.WorkoutData
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.main.MainActivity
import com.example.workoutProject.util.KeyboardUtil
import com.example.workoutproject.R
import kotlinx.android.synthetic.main.fragment_create_routine.view.*
import kotlinx.android.synthetic.main.top_bar.view.*

class CreateRoutineFragment : BaseFragment() {

    private val TAG = javaClass.simpleName
    lateinit var mContext: Context

    lateinit var top_title_view: TextView
    lateinit var top_right_button: Button
    lateinit var create_workout_title: TextView
    lateinit var create_checkbox_except_workout: CheckBox
    lateinit var create_set_picker: NumberPicker
    lateinit var create_prep_time_picker: NumberPicker
    lateinit var create_workout_time_picker: NumberPicker
    lateinit var create_rest_time_picker: NumberPicker
    lateinit var create_Add_button: Button
    lateinit var workout_list: RecyclerView
    lateinit var workoutAdapter: WorkoutAdapter

    var setCount = 0
    var prepTime = 0
    var workoutTime = 0
    var restTime = 0
    var workout_list_data: ArrayList<WorkoutData> = arrayListOf()

    companion object {
        var instance: CreateRoutineFragment? = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_routine, container, false)
        initView(view)
        initRoutineList()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun initView(view: View) {
        top_title_view = view.top_title_text
        top_right_button = view.top_right_button
        workout_list = view.create_list
        top_title_view.setText("Create Routine")
        top_right_button.setText("SAVE")
        top_right_button.setOnClickListener {
            if (workoutAdapter.infoList.size > 0) {
                // 저장
                popFragment()
                RoutineFragment.instance!!.onResume()
            } else {
                Toast.makeText(context, "Add Workout", Toast.LENGTH_LONG)
                    .show()
            }
        }

        create_workout_title = view.create_workout_title
        create_workout_title.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                KeyboardUtil.hideKeyboard(mContext, create_workout_title)
                true
            }
            false
        }
        create_workout_title.setOnEditorActionListener { textView, actionId, keyEvent ->
            when(actionId){
                EditorInfo.IME_ACTION_DONE -> {
                    KeyboardUtil.hideKeyboard(mContext, create_workout_title)
                    true
                }
                else -> false
            }
        }
        create_checkbox_except_workout = view.create_checkbox_except_workout
        create_set_picker = view.create_set_picker
        create_set_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        create_set_picker.minValue = 0
        create_set_picker.maxValue = 100
        create_set_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            setCount = i2
            Log.d(TAG, "setCount: $setCount")
        }
        create_prep_time_picker = view.create_prep_time_picker
        create_prep_time_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        create_prep_time_picker.minValue = 0
        create_prep_time_picker.maxValue = 60
        create_prep_time_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            prepTime = i2
            Log.d(TAG, "prepTime: $prepTime")
        }
        create_workout_time_picker = view. create_workout_time_picker
        create_workout_time_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        create_workout_time_picker.minValue = 0
        create_workout_time_picker.maxValue = 3600
        create_workout_time_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            workoutTime = i2
            Log.d(TAG, "workoutTime: $workoutTime")
        }
        create_checkbox_except_workout.setOnClickListener {
            create_workout_time_picker.isEnabled = !create_checkbox_except_workout.isChecked
        }
        create_rest_time_picker = view.create_rest_time_picker
        create_rest_time_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        create_rest_time_picker.minValue = 0
        create_rest_time_picker.maxValue = 3600
        create_rest_time_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            restTime = i2
            Log.d(TAG, "restTime: $restTime")
        }
        create_Add_button = view.create_Add_button
        create_Add_button.setOnClickListener {
            Log.d(TAG, "title: ${create_workout_title.text}")
            if (create_workout_title.text.toString() != "") {
//                if (!create_workout_time_picker.isEnabled) {
//                    workoutTime = 0
//                }
                val workoutData = WorkoutData(
                    create_workout_title.text.toString(),
                    setCount,
                    prepTime,
                    workoutTime,
                    restTime
                )
                workout_list_data.add(workoutData)
                workoutAdapter.setCurrentWork(workout_list_data)
            } else {
                Toast.makeText(context, "Enter the title", Toast.LENGTH_LONG)
                    .show()
                KeyboardUtil.openKeyboard(mContext, create_workout_title)
            }
            refreshView()
        }
    }

    fun initRoutineList() {
        workoutAdapter = WorkoutAdapter(mContext)
        workout_list.adapter = workoutAdapter
        workout_list.layoutManager = LinearLayoutManager(mContext)
        workout_list.setHasFixedSize(true)
        workoutAdapter.setCurrentWork(workout_list_data)
        workoutAdapter!!.itemClick = object :
            WorkoutAdapter.ItemClick {
            override fun onClick(view: View, position: Int) {
                Log.d(TAG, "${workout_list_data[position].getTitle()} 선택")
            }
        }
    }

    fun refreshView() {
        create_workout_title.setText("")
        create_checkbox_except_workout.isChecked = false
        create_set_picker.value = 0
        create_prep_time_picker.value = 0
        create_workout_time_picker.value = 0
        create_rest_time_picker.value = 0
        setCount = 0
        prepTime = 0
        workoutTime = 0
        restTime = 0
    }


}