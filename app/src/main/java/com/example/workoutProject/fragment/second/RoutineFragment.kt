package com.example.workoutProject.fragment.second

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.model.RoutineDateLiveData
import com.example.workoutProject.main.MainActivity
import com.example.workoutproject.R
import com.example.workoutproject.data.RoutineColor
import com.example.workoutproject.data.RoutineData
import kotlinx.android.synthetic.main.fragment_routine.view.*
import kotlinx.android.synthetic.main.top_bar.view.*

class RoutineFragment : BaseFragment() {

    private val TAG = javaClass.simpleName
    lateinit var mContext: Context
    var date = ""

    companion object {
        var instance: RoutineFragment? = null
    }

    lateinit var routine_list: RecyclerView
    lateinit var routineAdapter: RoutineAdapter
    var routine_list_data: List<RoutineData> = listOf(
        RoutineData("title1", RoutineColor.RED,1, 100),
        RoutineData("title2", RoutineColor.ORANGE,2, 120),
        RoutineData("title3", RoutineColor.YELLOW,3, 140),
        RoutineData("title4", RoutineColor.GREEN,4, 160),
        RoutineData("title5", RoutineColor.BLUE,5, 180)
    )
    lateinit var routine_date_layout: LinearLayout

    lateinit var top_title_view: TextView
    lateinit var top_right_button: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_routine, container, false)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        RoutineDateLiveData.getInstance().getLiveProgress().observe(this, Observer<String> {
            Log.d(TAG, "date: $it")
            date = it
            if (date == "") {
                routine_date_layout.visibility = View.GONE
            } else {
                routine_date_layout.visibility = View.VISIBLE
            }
        })
        initRoutineList()
    }

    fun initView(view: View) {
        Log.d(TAG, "initView")
        top_title_view = view.top_title_text
        top_right_button = view.top_right_button
        routine_date_layout = view.routine_date_layout
        routine_list = view.routine_list
        top_title_view.setText("Routine List")
        top_right_button.setText("+")
        top_right_button.setOnClickListener {
            // Routine - Round - Set
            SecondFragment.instance!!.addFragment(CreateRoutineFragment(), R.id.second_content_frame, "createroutine")
        }
    }

    fun initRoutineList() {
        routineAdapter = RoutineAdapter(mContext)
        routine_list.adapter = routineAdapter
        routine_list.layoutManager = LinearLayoutManager(mContext)
        routine_list.setHasFixedSize(true)
        routineAdapter.setCurrentWork(routine_list_data)
        routineAdapter!!.itemClick = object :
            RoutineAdapter.ItemClick {
            override fun onClick(view: View, position: Int) {
                Log.d(TAG, "${routine_list_data[position].getTitle()} 선택")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
}