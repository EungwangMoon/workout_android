package com.example.workoutProject.fragment.fourthtimertest

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TimerViewModel : ViewModel() {
    private val TAG = javaClass.simpleName

    var timer: CountDownTimer? = null
    var progressTimer: CountDownTimer? = null
    val currentTime = MutableLiveData<Long>()
    val currentProgress = MutableLiveData<Long>()
    val isRunning = MutableLiveData<Boolean>()
    val ONE_SECOND = 1000L
    val DONE = 0L
    var COUNTDOWN_TIMER = 0L

    fun setTimer() {
        Log.d(TAG, "setTimer")
        isRunning.value = true
        timer = object: CountDownTimer(COUNTDOWN_TIMER, ONE_SECOND) {
            override fun onTick(milliSecondFinished: Long) {
                COUNTDOWN_TIMER = milliSecondFinished
                currentProgress.value = COUNTDOWN_TIMER
                currentTime.value = COUNTDOWN_TIMER / ONE_SECOND + 1
            }
            override fun onFinish() {
                Log.d(TAG, "timer onFinish")
                isRunning.value = false
                COUNTDOWN_TIMER = 0L
                currentTime.value = DONE
            }
        }
        progressTimer = object: CountDownTimer(COUNTDOWN_TIMER, 1) {
            override fun onTick(milliSecondFinished: Long) {
                COUNTDOWN_TIMER = milliSecondFinished
                currentProgress.value = COUNTDOWN_TIMER
            }
            override fun onFinish() {
                Log.d(TAG, "progressTimer onFinish")
                isRunning.value = false
                COUNTDOWN_TIMER = 0L
                currentTime.value = DONE
            }
        }
    }

    fun startTimer(countDownTime: Long) {
        // 20000(20초)를 넣으면 바로 19가 되기 때문에
        // countDownTime은 +1000 된 값으로 넘겨야함
        Log.d(TAG, "startTimer")
//        isRunning.value = false
        COUNTDOWN_TIMER = countDownTime
        setTimer()
        timer?.start()
        progressTimer?.start()
    }

    fun pauseTimer() {
        timer?.cancel()
        progressTimer?.cancel()
    }

    fun resetTimer() {
        isRunning.value = false
        timer?.cancel()
        progressTimer?.cancel()
        COUNTDOWN_TIMER = 0L
        currentTime.value = DONE
    }

    override fun onCleared() {
        super.onCleared()
        timer?.cancel()
        progressTimer?.cancel()
    }
}