package com.example.workoutProject.fragment.fourth

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.fragment.fourthtimertest.TimerViewModel
import com.example.workoutProject.main.MainActivity
import com.example.workoutProject.util.PreferenceUtil
import com.example.workoutproject.R
import kotlinx.android.synthetic.main.fragment_timer.*


class TimerFragment : BaseFragment() {
    private val TAG = javaClass.simpleName

    lateinit var mContext: Context
    lateinit var timerViewModel: TimerViewModel

    // SharedPreference Key
    val originTimeKey = "ORIGIN_TIME"
    val isPauesedKey = "IS_PAUSED"
    var isPaused = false

    companion object {
        var instance: TimerFragment? = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_timer, container, false)
        timerViewModel = ViewModelProvider(this).get(TimerViewModel::class.java)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var progressMax = PreferenceUtil(mContext).getInt(originTimeKey, 0)
        Log.e(TAG, "getProgressMax: $progressMax")
        if (progressMax == 0) {
            progressMax = 3600
        }
        timer_progress.max = progressMax
        timer_progress.progress = timer_progress.max

        isPaused = PreferenceUtil(mContext).getBoolean(isPauesedKey, false)

        timerViewModel.currentProgress.observe(viewLifecycleOwner, Observer {
            timer_progress.progress = it.toInt()
        })
        timerViewModel.currentTime.observe(viewLifecycleOwner, Observer {
            val min = it / 60
            val sec = it % 60
            if (min < 10) {
                timer_minute_text.text = "0$min"
            } else {
                timer_minute_text.text = min.toString()
            }
            if (sec < 10) {
                timer_second_text.text = "0$sec"
            } else {
                timer_second_text.text = sec.toString()
            }
        })
        timerViewModel.isRunning.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "isRunning: $it")
            if (it == false) {
                // Reset 상태
                timer_picker_layout.visibility = View.VISIBLE
                timer_text_layout.visibility = View.GONE
                timer_start_button.visibility = View.VISIBLE
                timer_stop_button.visibility = View.GONE
                timer_progress.progress = timer_progress.max
            } else {
                // Start, Stop 상태
                Log.d(TAG, "isPaused: $isPaused")
                timer_picker_layout.visibility = View.GONE
                timer_text_layout.visibility = View.VISIBLE
                timer_minute_picker.value = 0
                timer_second_picker.value = 0
                if (isPaused) {
                    timer_start_button.visibility = View.VISIBLE
                    timer_stop_button.visibility = View.GONE
                } else {
                    timer_start_button.visibility = View.GONE
                    timer_stop_button.visibility = View.VISIBLE
                }
            }
        })
        timer_minute_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        timer_minute_picker.minValue = 0
        timer_minute_picker.maxValue = 59
        timer_second_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        timer_second_picker.minValue = 0
        timer_second_picker.maxValue = 59
        timer_minute_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            Log.d(TAG, "minute picker value: $i")
        }
        timer_second_picker.setOnValueChangedListener { numberPicker, i, i2 ->
            Log.d(TAG, "second picker value: $i")
        }
        timer_reset_button.setOnClickListener {
            timerViewModel.resetTimer()
            timer_progress.progress = timer_progress.max
        }
        timer_start_button.setOnClickListener {
            var countDownTime = 0L
            if (timerViewModel.COUNTDOWN_TIMER > 0L) {
                Log.d(TAG, "1111111111")
                countDownTime = timerViewModel.COUNTDOWN_TIMER
            } else {
                PreferenceUtil(mContext).setInt(originTimeKey, 0)
                Log.d(TAG, "2222222222")
                countDownTime = ((timer_minute_picker.value * 60 + timer_second_picker.value) * 1000).toLong()
                timer_progress.max = ((timer_minute_picker.value * 60 + timer_second_picker.value) * 1000)
                PreferenceUtil(mContext).setInt(originTimeKey, timer_progress.max)
                Log.e(TAG, "timer_progress.max: ${timer_progress.max}")
            }
            if (countDownTime > 0) {
                isPaused = false
                PreferenceUtil(mContext).setBoolean(isPauesedKey, isPaused)
                Log.d(TAG, "COUNTDOWN_TIMER: ${timerViewModel.COUNTDOWN_TIMER}")
                timerViewModel.startTimer(countDownTime)
                timer_start_button.visibility = View.GONE
                timer_stop_button.visibility = View.VISIBLE
            } else {
                timer_progress.max = 3600
                timer_progress.progress = timer_progress.max
                return@setOnClickListener
            }
        }
        timer_stop_button.setOnClickListener {
            timer_start_button.visibility = View.VISIBLE
            timer_stop_button.visibility = View.GONE
            timerViewModel.pauseTimer()
            isPaused = true
            PreferenceUtil(mContext).setBoolean(isPauesedKey, isPaused)
            Log.e(TAG, "timer_progress.max: ${timer_progress.max}")
            PreferenceUtil(mContext).setInt(originTimeKey, timer_progress.max)
            Log.d(TAG, "currentTime: ${timerViewModel.currentTime.value}")
            Log.d(TAG, "COUNTDOWN_TIMER: ${timerViewModel.COUNTDOWN_TIMER}")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        instance = null
    }
}