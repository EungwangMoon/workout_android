package com.example.workoutProject.fragment.second

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutProject.model.WorkoutData
import com.example.workoutproject.R

class WorkoutAdapter (val context: Context) :
    RecyclerView.Adapter<WorkoutAdapter.WorkoutItemHolder>() {

    private val TAG = javaClass.simpleName
    var infoList: List<WorkoutData> = listOf()

    interface ItemClick {
        fun onClick(view: View, position: Int)
    }

    var itemClick: ItemClick? = null

    override fun onBindViewHolder(holder: WorkoutItemHolder, position: Int) {
        holder?.bind(infoList[position], context)
        if (itemClick != null) {
            holder?.itemView?.setOnClickListener { v ->
                itemClick?.onClick(v, position)

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutItemHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.list_item_workout, parent, false)
        return WorkoutItemHolder(view)
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "infoList size : " + infoList.size)
        return infoList.size
    }

    fun setCurrentWork(holdListData: List<WorkoutData>) {
        infoList = holdListData
        notifyDataSetChanged()
    }

    inner class WorkoutItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        var workout_item_title_text: TextView = itemView!!.findViewById(R.id.workout_item_title_text)
        var workout_item_set_text: TextView = itemView!!.findViewById(R.id.workout_item_set_text)
        var workout_item_prep_time_text: TextView = itemView!!.findViewById(R.id.workout_item_prep_time_text)
        var workout_item_workout_time_text: TextView = itemView!!.findViewById(R.id.workout_item_workout_time_text)
        var workout_item_rest_time_text: TextView = itemView!!.findViewById(R.id.workout_item_rest_time_text)

        fun bind(data: WorkoutData, context: Context) {
            workout_item_title_text.text = data.getTitle()
            workout_item_set_text.setText(context.getString(R.string.create_set_text, data.getSet().toString()))
            workout_item_prep_time_text.setText(context.getString(R.string.create_prep_text, data.getPrep().toString()))
            workout_item_workout_time_text.setText(context.getString(R.string.create_workout_text, data.getWorkout().toString()))
            workout_item_rest_time_text.setText(context.getString(R.string.create_rest_text, data.getRest().toString()))
        }

    }

}