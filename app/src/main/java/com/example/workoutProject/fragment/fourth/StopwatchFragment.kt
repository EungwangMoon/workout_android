package com.example.workoutProject.fragment.fourth

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.workoutProject.common.BaseFragment
import com.example.workoutProject.main.MainActivity
import com.example.workoutproject.R
import com.example.workoutproject.data.StopwatchRecordData
import kotlinx.android.synthetic.main.fragment_stop_watch.*
import kotlinx.android.synthetic.main.fragment_stop_watch.view.*
import java.util.*

class StopwatchFragment : BaseFragment() {

    private val TAG = javaClass.simpleName
    private lateinit var mContext: Context

    lateinit var record_list: RecyclerView
    lateinit var recordAdapter: StopwatchRecordAdapter

    private var isRunning = false
    private var timerTask: Timer? = null
    private var time = 0

    companion object {
        var instance: StopwatchFragment? = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mContext = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_stop_watch, container, false)
        record_list = view.stopwatch_record_list
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        stopwatch_start_button.setOnClickListener {
            Log.d(TAG, "isRunning: $isRunning")
            isRunning = !isRunning
            Log.d(TAG, "isRunning: $isRunning")
            if (isRunning) {
                start()
            } else {
                pause()
            }
        }
        stopwatch_lap_button.setOnClickListener {
            lap()
        }
    }

    private fun initView() {
        stopwatch_minute_text.text = "00"
        stopwatch_second_text.text = "00"
        stopwatch_milisecond_text.text = "00"

        recordAdapter = StopwatchRecordAdapter(mContext)
        record_list.adapter = recordAdapter
        val recordLayoutManager = LinearLayoutManager(mContext)
        recordLayoutManager.reverseLayout = true
        recordLayoutManager.stackFromEnd = true
        record_list.layoutManager = recordLayoutManager
        record_list.setHasFixedSize(true)
    }

    private fun start() {
        stopwatch_start_button.text = "STOP"
        stopwatch_lap_button.text = "LAP"
        timerTask = kotlin.concurrent.timer(period = 10) {
            time++

            val milisec = time % 100
            val sec = time / 100
            val min = sec / 60
            activity?.runOnUiThread  {
                if (min < 10) {
                    stopwatch_minute_text.text = "0$min"
                } else {
                    stopwatch_minute_text.text = "$min"
                }
                if (sec < 10) {
                    stopwatch_second_text.text = "0$sec"
                } else {
                    stopwatch_second_text.text = "$sec"
                }
                if (milisec < 10) {
                    stopwatch_milisecond_text.text = "0$milisec"
                } else {
                    stopwatch_milisecond_text.text = "$milisec"
                }
            }
        }
    }

    private fun pause() {
        stopwatch_start_button.text = "START"
        stopwatch_lap_button.text = "RESET"
        timerTask?.cancel()
    }

    private fun lap() {
        if (isRunning) {
            // 기록상태
            val recordSize = recordAdapter.infoList.size
            val lapData = StopwatchRecordData("Lap $recordSize", "${stopwatch_minute_text.text}:${stopwatch_second_text.text}.${stopwatch_milisecond_text.text}")
            Log.d(TAG, "title: ${lapData.getTitle()}, record: ${lapData.getRecord()}")
            recordAdapter.addLap(lapData)
            record_list.scrollToPosition(recordSize)
        } else if (!isRunning) {
            // 정지상태
            timerTask?.cancel()
            isRunning = false
            time = 0
            stopwatch_minute_text.text = "00"
            stopwatch_second_text.text = "00"
            stopwatch_milisecond_text.text = "00"
            stopwatch_start_button.text = "Start"
            initView()
        }
    }
}